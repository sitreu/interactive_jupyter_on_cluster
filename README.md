# Run Jupyter on a cluster compute node

This script starts an instance of `jupyter-notebook` (or, optionally, `jupyter-lab`) on a cluster compute node
and automatically performs all the required steps to access it from your local browser.

## Setup

MacOS users need to install `sha256sum` and `GNU grep` using the command line `brew install sha256sum grep`.

Make sure to set at least the following in the `slurm_jupyter.cfg`:

* cluster login user name (`user`),
* cluster working directory (`work_dir`),
* cluster conda environment (`conda_env`),
* SLURM account (`slurm_account`).
* Add a file called `config` to `~/.ssh` with the following content (replace your_username):
```bash
ForwardX11Trusted yes

Host hpc_proxy
     Hostname login01-24.pik-potsdam.de
     Proxyjump ts01
     User your_username
     ForwardX11 yes
Host ts01
     Hostname ts01.pik-potsdam.de
     User your_username
     ForwardX11 yes
```

Execute the command line `sacctmgr list user -s $USER` on a login node to get a list of your associated SLURM accounts.
Make sure that the package `notebook` (or, optionally, `jupyterlab`) is installed in your specified conda environment.

As another prerequisit, you need to be able to switch between the two login nodes (`login01` and `login02`) using `ssh login02` when logged in on `login01` and vice versa. If this is not possible, or if you are asked for a passphrase, you might want to create a new SSH key on the cluster by running `ssh-keygen` (without passphrase!). Make sure to add the new public key (in `$HOME/.ssh/id_rsa.pub` on the cluster) to the `$HOME/.ssh/authorized_keys` file on the cluster.

## How to run in three steps

1. Execute `./cluster_notebook.sh` on your local machine.
2. Wait for the Jupyter job to start.
3. Paste the URL given in the terminal output to your browser to start the Jupyter user interface.

## Stopping the Jupyter server

When you are done, press `CTRL+C` in your terminal and confirm (either by pressing `CTRL+C` again or by pressing `y`
and `<RETURN>` or `<ENTER>`). The script will take care of cleaning up and canceling the SLURM job.

If your local `./cluster_notebook.sh`-process is killed or you lose connection to the server (so that the SLURM job is
not canceled) you can rerun `./cluster_notebook.sh` at any later time to re-connect to the existing SLURM job.

## To do list

* Optionally, create new Conda environment on start up.
* Optionally, check out some git repository on start up.
* Optionally, choose Conda environment according to info in selected git repository.
