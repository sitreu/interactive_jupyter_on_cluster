#!/bin/bash

# DO NOT CHANGE THIS FILE!
# Use the slurm_jupyter.cfg file instead

source slurm_jupyter.cfg

gnu_grep=$(which ggrep || which grep)
echo "Using $($gnu_grep --version | head -1)"

jobid=""
# ssh_base=${user}@foote.pik-potsdam.de
ssh_base=hpc_proxy
ssh_tunnel_command=''
sigint_flag=0

ctrl_c()
{
    sigint_flag=1
}

# copy slurm script to working directory on cluster
slurm_script="${work_dir}/.jupyter_on_cluster.sh"
ssh $ssh_base "mkdir -p $work_dir" &>/dev/null
rsync jupyter_on_cluster.sh "$ssh_base:$slurm_script" &>/dev/null

trap cleanup SIGINT SIGTERM

cleanup()
{
    echo ''
    if [ -z "$jobid" ]; then
        echo "Script canceled before cluster job was started."
    else
        rm -rf $(basename $slurm_output)
        echo "Canceling cluster job $jobid..."
        ssh $ssh_base "scancel $jobid; \
                       rm $slurm_output $slurm_script" &>/dev/null
    fi
    echo 'Goodbye.'
    exit 1
}

ctrl_c_prompt()
{
    if [ $sigint_flag -eq 1 ]; then
        sigint_flag=0
        echo ''
        trap cleanup SIGINT
        read -t 5 -r -p "Would you like to cancel the cluster job (y/[n])?  "
        if [ $? -gt 128 ]; then
            echo "No answer for 5s: resuming operation..."
        elif [[ $REPLY =~ ^([yY]) ]] || [ $sigint_flag -eq 1 ]; then
            cleanup
        else
            echo "resuming operation..."
        fi
        trap ctrl_c SIGINT
    fi
}

slurm_command="sbatch --chdir=$work_dir \
                      --account=$slurm_account \
                      --qos=$slurm_qos \
                      --job-name=$slurm_job_name \
                      --time=$slurm_time \
                      --output=SLURM_OUTPUT \
                      $slurm_extra_params \
                      $slurm_script $conda $conda_env $lab_or_notebook"

# include the command's hash in the log file name to identify already running jobs
slurm_command_hash=`echo "$slurm_command" | sha256sum | awk '{print($1)}'`
slurm_output="${work_dir}/.jupyter-${slurm_command_hash}-%j.log"
slurm_command=`echo $slurm_command | sed "s/SLURM_OUTPUT/$(basename $slurm_output)/g"`

# script on cluster tries to find existing jobs with same hash
jobid=`ssh $ssh_base "cd $work_dir; /bin/bash $slurm_script $slurm_command_hash" 2>/dev/null`
if [ -n "$jobid" ]; then
    echo -n "Obtaining info about existing Jupyter job $jobid..."
else
    # submit a new job
    jobid=`ssh $ssh_base "$slurm_command" 2>/dev/null | awk '{print($4)}'`
    echo "Submitted cluster job $jobid"
    echo -n "Waiting for Jupyter job to start..."
fi
slurm_output=`echo $slurm_output | sed "s/%j/$jobid/g"`

# create a local copy of the SLURM log file
touch $(basename $slurm_output)

# Now that the job started, we react to CTRL+C by asking for confirmation to cancel the job
trap ctrl_c SIGINT

i_wait=3
while [ -z "$ssh_tunnel_command" ]; do
    [ $((i_wait++ % 5)) == 0 ] && echo -ne "\b\b\b\b\b     \b\b\b\b\b"
    echo -n "."
    ctrl_c_prompt
    sleep 1
    rsync $ssh_base:$slurm_output $(basename $slurm_output) &>/dev/null
    if $gnu_grep -q "Activating Conda environment failed!" $(basename $slurm_output); then
        echo ""
        echo "Activating Conda in $conda_env failed!"
        cleanup
        exit 1
    elif $gnu_grep -q "Jupyter notebook won't work" $(basename $slurm_output); then
        echo ""
        $gnu_grep "Jupyter notebook won't work" $(basename $slurm_output)
        echo "Fix permissions or use Jupyter 'lab' instead."
        cleanup
        exit 1
    fi
    ssh_tunnel_command=`$gnu_grep -m1 "ssh -L" $(basename $slurm_output)`
done
notebook_port=${ssh_tunnel_command:6:4}
notebook_internal_url=''
while [ -z "$notebook_internal_url" ]; do
    [ $((i_wait++ % 5)) == 0 ] && echo -ne "\b\b\b\b\b     \b\b\b\b\b"
    echo -n "."
    ctrl_c_prompt
    sleep .5
    rsync $ssh_base:$slurm_output $(basename $slurm_output) &>/dev/null
    if $gnu_grep -q "srun: error" $(basename $slurm_output); then
        echo ""
        if $gnu_grep -q "\`jupyter-${lab_or_notebook}\` not found." $(basename $slurm_output); then
            echo "Jupyter job failed because $lab_or_notebook is not installed in Conda env."
        else
            echo "Jupyter job failed with error output:"
            sed -n '/srun: error/,$p' $(basename $slurm_output)
        fi
        cleanup
        exit 1
    fi
    notebook_internal_url=`$gnu_grep -oP -m1 'http://localhost:[0-9]{4}/(lab)?(\?token=[a-z0-9]+)?' $(basename $slurm_output)`
done
notebook_lab=""
notebook_token=""
if [[ $notebook_internal_url == *"/lab"* ]]; then
    notebook_lab="lab"
fi
if [[ $notebook_internal_url == *"token="* ]]; then
    notebook_token=`echo $notebook_internal_url | $gnu_grep -oP '(?<=token=)([a-z0-9]+)'`
    notebook_token="?token=$notebook_token"
fi
echo ""
echo "Point your browser to http://localhost:${notebook_port}/${notebook_lab}${notebook_token}"

ssh_tunnel_port=`echo $ssh_tunnel_command | $gnu_grep -oP -m1 '(?<=-L)([0-9]+)'`
if lsof -i -P -n | $gnu_grep -q ":$ssh_tunnel_port[^0-9]"; then
    echo "SSH port forwarding is already running in a different process on this machine."
else
    while true; do
        ctrl_c_prompt
        $ssh_tunnel_command
        if [ $sigint_flag -eq 0 ]; then
            echo ""
            echo "The SSH tunnel closed unexpectedly."
            read -r -p "Would you like to reactivate the tunnel ([y]/n)?  "
            if [[ $REPLY =~ ^([nN]) ]]; then
                sigint_flag=1
            fi
        fi
    done
fi
