#!/bin/bash

# DO NOT CHANGE THIS FILE!
# Use the slurm_jupyter.cfg file instead

user=`whoami`

if [ -z "$SLURM_SUBMIT_HOST" ]; then
    # the script is not run with sbatch, but directly from the console
    # look for already running jobs with given hash
    jobid=""
    shopt -s nullglob
    for f in .jupyter-$2-*.log; do
        jobid=`echo $f | cut -d. -f2 | cut -d- -f3`
        jobid=`squeue -u $user | grep -q $jobid && echo $jobid`
    done
    if [ -n "$jobid" ]; then
        echo $jobid
    fi
    exit 0
fi

# Some initial setup
module purge

if [ -z "$1" ]; then
    echo "The variable conda must be specified in the settings"
elif [ "$1" == "load" ]; then
    echo "Using your custom conda installation"
else
    module load "$1"
fi

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export MKL_NUM_THREADS=$SLURM_CPUS_PER_TASK
export NUMEXPR_NUM_THREADS=$SLURM_CPUS_PER_TASK

# Load a Julia module, if you're running Julia notebooks
#module load julia/1.0.0
# Load an R module, if you're running R notebooks. See README.md for setup instructions
#module load R/3.5.1 intel/2018.1
# Load additional modules if you need them
#module load matlab/R2016b

if source activate $2; then
    echo "Successfully activated Conda environment in $2"
else
    echo "Activating Conda environment failed!"
    exit 1
fi

JUPYTER_CMD="$CONDA_PREFIX/bin/jupyter"

if [ "$3" == "notebook" ]; then
    # "notebook" needs read access to the template folder in the Conda environment
    for i in $CONDA_PREFIX/share/jupyter/nbconvert/templates/*; do
        if [ ! -r "$i" ]; then
            echo "Jupyter notebook won't work because of missing read permissions for $i"
            exit 1
        fi
    done
fi

# set a random port for the notebook, in case multiple notebooks are
# on the same compute node.
NOTEBOOKPORT=`shuf -i 8000-8500 -n 1`

# set a random port for tunneling, in case multiple connections are happening
# on the same login node.
TUNNELPORT=`shuf -i 8501-9000 -n 1`

echo "ssh -L$NOTEBOOKPORT:localhost:$TUNNELPORT ${user}@$SLURM_SUBMIT_HOST-24.pik-potsdam.de -J ${user}@ts01.pik-potsdam.de -N"

# Set up a reverse SSH tunnel from the compute node back to the submitting host (login01 or login02)
# This is the machine we will connect to with SSH forward tunneling from our client.
ssh -R$TUNNELPORT:localhost:$NOTEBOOKPORT $SLURM_SUBMIT_HOST -N -f

# Start the notebook
echo "Starting Jupyter '$3' from $JUPYTER_CMD ..."
srun -n1 $JUPYTER_CMD $3 --no-browser --port=$NOTEBOOKPORT
